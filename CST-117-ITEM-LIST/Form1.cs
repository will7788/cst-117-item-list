﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_117_ITEM_LIST
{
    public partial class Form1 : Form
    {
        private Dictionary<string, string[]> itemDict = new Dictionary<string, string[]>();
        public Form1()
        {
            InitializeComponent();
            string[] itemArray = items.Items.OfType<string>().ToArray();
            Random rnd = new Random();
            foreach (var str in itemArray)
            {
                itemDict.Add(str, new string[5] {rnd.Next(1,100) + "", 100 + "", "#" + rnd.Next(100000,999999) + "",
                    "$" + rnd.Next(1,1000) + "." + rnd.Next(1, 100), rnd.Next(1, 13) + "/" + rnd.Next(1, 29) + "/" + rnd.Next(2019, 2030) });
            }
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPrice.Checked)
            {
                itemPrice.Visible = true;
                label5.Visible = true;
            }
            else
            {
                itemPrice.Visible = false;
                label5.Visible = false;
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Items_SelectedIndexChanged(object sender, EventArgs e)
        {
            nameDisplay.Text = items.SelectedItem.ToString();
            if (itemDict.ContainsKey(items.SelectedItem.ToString()))
            {
                string[] temp = itemDict[items.SelectedItem.ToString()];
                currentStock.Text = temp[0];
                maxStock.Text = temp[1];
                itemNumber.Text = temp[2];
                itemPrice.Text = temp[3];
                expDate.Text = temp[4];
            }

        }

        private void NameDisplay_Click(object sender, EventArgs e)
        {

        }

        private void RadioShow_CheckedChanged(object sender, EventArgs e)
        {
            if (radioShow.Checked)
            {
                panel1.Visible = true;
            }
        }

        private void CheckExp_CheckedChanged(object sender, EventArgs e)
        {
            if (checkExp.Checked)
            {
                expDate.Visible = true;
                label6.Visible = true;
            }
            else
            {
                expDate.Visible = false;
                label6.Visible = false;
            }
        }

        private void RadioHide_CheckedChanged(object sender, EventArgs e)
        {
            if (radioHide.Checked)
            {
                panel1.Visible = false;
            }
        }
    }
}
