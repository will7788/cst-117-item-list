﻿namespace CST_117_ITEM_LIST
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.items = new System.Windows.Forms.ListBox();
            this.radioShow = new System.Windows.Forms.RadioButton();
            this.checkPrice = new System.Windows.Forms.CheckBox();
            this.radioHide = new System.Windows.Forms.RadioButton();
            this.checkExp = new System.Windows.Forms.CheckBox();
            this.nameDisplay = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.currentStock = new System.Windows.Forms.TextBox();
            this.maxStock = new System.Windows.Forms.TextBox();
            this.itemNumber = new System.Windows.Forms.TextBox();
            this.itemPrice = new System.Windows.Forms.TextBox();
            this.expDate = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioShow);
            this.groupBox1.Controls.Add(this.radioHide);
            this.groupBox1.Location = new System.Drawing.Point(12, 182);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 67);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Display Properties";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkExp);
            this.groupBox2.Controls.Add(this.checkPrice);
            this.groupBox2.Location = new System.Drawing.Point(12, 255);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 68);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // items
            // 
            this.items.FormattingEnabled = true;
            this.items.Items.AddRange(new object[] {
            "Ranch Dressing",
            "Pepperoni",
            "Sausage",
            "Dough",
            "Red Sauce",
            "Cheese",
            "Tomato",
            "Linguica"});
            this.items.Location = new System.Drawing.Point(12, 12);
            this.items.Name = "items";
            this.items.Size = new System.Drawing.Size(200, 160);
            this.items.TabIndex = 0;
            this.items.SelectedIndexChanged += new System.EventHandler(this.Items_SelectedIndexChanged);
            // 
            // radioShow
            // 
            this.radioShow.AutoSize = true;
            this.radioShow.Checked = true;
            this.radioShow.Location = new System.Drawing.Point(6, 19);
            this.radioShow.Name = "radioShow";
            this.radioShow.Size = new System.Drawing.Size(52, 17);
            this.radioShow.TabIndex = 1;
            this.radioShow.TabStop = true;
            this.radioShow.Text = "Show";
            this.radioShow.UseVisualStyleBackColor = true;
            this.radioShow.CheckedChanged += new System.EventHandler(this.RadioShow_CheckedChanged);
            // 
            // checkPrice
            // 
            this.checkPrice.AutoSize = true;
            this.checkPrice.Checked = true;
            this.checkPrice.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkPrice.Location = new System.Drawing.Point(5, 19);
            this.checkPrice.Name = "checkPrice";
            this.checkPrice.Size = new System.Drawing.Size(103, 17);
            this.checkPrice.TabIndex = 2;
            this.checkPrice.Text = "Show Item Price";
            this.checkPrice.UseVisualStyleBackColor = true;
            this.checkPrice.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // radioHide
            // 
            this.radioHide.AutoSize = true;
            this.radioHide.Location = new System.Drawing.Point(6, 42);
            this.radioHide.Name = "radioHide";
            this.radioHide.Size = new System.Drawing.Size(47, 17);
            this.radioHide.TabIndex = 3;
            this.radioHide.Text = "Hide";
            this.radioHide.UseVisualStyleBackColor = true;
            this.radioHide.CheckedChanged += new System.EventHandler(this.RadioHide_CheckedChanged);
            // 
            // checkExp
            // 
            this.checkExp.AutoSize = true;
            this.checkExp.Checked = true;
            this.checkExp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkExp.Location = new System.Drawing.Point(5, 42);
            this.checkExp.Name = "checkExp";
            this.checkExp.Size = new System.Drawing.Size(103, 17);
            this.checkExp.TabIndex = 4;
            this.checkExp.Text = "Show Exp. Date";
            this.checkExp.UseVisualStyleBackColor = true;
            this.checkExp.CheckedChanged += new System.EventHandler(this.CheckExp_CheckedChanged);
            // 
            // nameDisplay
            // 
            this.nameDisplay.AutoSize = true;
            this.nameDisplay.Location = new System.Drawing.Point(68, 10);
            this.nameDisplay.Name = "nameDisplay";
            this.nameDisplay.Size = new System.Drawing.Size(58, 13);
            this.nameDisplay.TabIndex = 5;
            this.nameDisplay.Text = "Item Name";
            this.nameDisplay.Click += new System.EventHandler(this.NameDisplay_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Current Stock:";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.expDate);
            this.panel1.Controls.Add(this.itemPrice);
            this.panel1.Controls.Add(this.itemNumber);
            this.panel1.Controls.Add(this.maxStock);
            this.panel1.Controls.Add(this.currentStock);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.nameDisplay);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(218, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(266, 311);
            this.panel1.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Max Stock:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Item Number:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Item Price:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Expiration Date:";
            // 
            // currentStock
            // 
            this.currentStock.Location = new System.Drawing.Point(91, 27);
            this.currentStock.Name = "currentStock";
            this.currentStock.ReadOnly = true;
            this.currentStock.Size = new System.Drawing.Size(167, 20);
            this.currentStock.TabIndex = 11;
            this.currentStock.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // maxStock
            // 
            this.maxStock.Location = new System.Drawing.Point(91, 57);
            this.maxStock.Name = "maxStock";
            this.maxStock.ReadOnly = true;
            this.maxStock.Size = new System.Drawing.Size(167, 20);
            this.maxStock.TabIndex = 12;
            // 
            // itemNumber
            // 
            this.itemNumber.Location = new System.Drawing.Point(91, 87);
            this.itemNumber.Name = "itemNumber";
            this.itemNumber.ReadOnly = true;
            this.itemNumber.Size = new System.Drawing.Size(167, 20);
            this.itemNumber.TabIndex = 13;
            // 
            // itemPrice
            // 
            this.itemPrice.Location = new System.Drawing.Point(91, 117);
            this.itemPrice.Name = "itemPrice";
            this.itemPrice.ReadOnly = true;
            this.itemPrice.Size = new System.Drawing.Size(167, 20);
            this.itemPrice.TabIndex = 14;
            // 
            // expDate
            // 
            this.expDate.Location = new System.Drawing.Point(91, 147);
            this.expDate.Name = "expDate";
            this.expDate.ReadOnly = true;
            this.expDate.Size = new System.Drawing.Size(167, 20);
            this.expDate.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 333);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.items);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox items;
        private System.Windows.Forms.RadioButton radioShow;
        private System.Windows.Forms.CheckBox checkPrice;
        private System.Windows.Forms.RadioButton radioHide;
        private System.Windows.Forms.CheckBox checkExp;
        private System.Windows.Forms.Label nameDisplay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox expDate;
        private System.Windows.Forms.TextBox itemPrice;
        private System.Windows.Forms.TextBox itemNumber;
        private System.Windows.Forms.TextBox maxStock;
        private System.Windows.Forms.TextBox currentStock;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}

